function Player() {
    playerP = new Sprite(scene, "./images/princess3.png", 230,300);
    playerP.setSpeed(0);
    playerP.setAngle(90);
    playerP.changeYby(430);

    playerP.motionFrong = function() {
        if (keysDown[K_LEFT] || keysDown[K_A]) {
            // ลูกศรซ้าย
            this.setMoveAngle(-90); // กำหนดองศาการเคลื่อนที่
            this.setSpeed(20); // กำหนดความเร็ว
          }
          if (keysDown[K_RIGHT] || keysDown[K_D]) {
            // ลูกศรขวา
            this.setMoveAngle(90); // กำหนดองศาการเคลื่อนที่
            this.setSpeed(20); // กำหนดความเร็ว
          }
          if (keysDown[K_DOWN] || keysDown[K_SPACE]) {
              // ลูกศรลง หรือ spacebar
              this.setSpeed(0); // กำหนดความเร็ว
              this.setMoveAngle(0); // กำหนดองศาการเคลื่อนที่
            }
    }
    return playerP;
}

function Candy() {
    moCandy = new Sprite(scene, "./images/candy.png", 100,100);

    moCandy.motionCandy = function() {
        this.changeYby(20);
        this.setMoveAngle(180);
        this.setSpeed(Math.random() * 5);
    }

    moCandy.dead = function() {
        newX = Math.random()*this.cWidth;
        newY = Math.random()*this.cHeight;
        this.setPosition(newX,newY);
    }
    return moCandy;
}

function Boom() {
    moBoom = new Sprite(scene, "./images/boommm.png", 100,100);
    moBoom.motionBoom = function() {
        this.changeYby(20);
        this.setMoveAngle(180);
        this.setSpeed(Math.random() * 2);
    }

    moBoom.boommer = function() {
        newX = Math.random()*this.cWidth;
        newY = Math.random()*this.cHeight;
        this.setPosition(newX,newY);
    }
    return moBoom;
}